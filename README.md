## Idea
There are two programs : socket server and socket client

- socket server :
 	- Doing inference in infinite loop then update last data to be displayed
 	- listen to socket with message 'detect', to send last data to React

- socket client :
	- trigger socket server to send last data periodically (see setInterval function)
	- display last data