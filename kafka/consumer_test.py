# python2
from confluent_kafka import Consumer, KafkaError
import sys
import time

settings = {
    'bootstrap.servers': 'localhost:9092',
    'group.id': sys.argv[2],
    'client.id': sys.argv[3],
    'enable.auto.commit': True,
    'session.timeout.ms': 6000,
    'default.topic.config': {'auto.offset.reset': 'smallest'}
}

c = Consumer(settings)

c.subscribe([sys.argv[1]])

try:
    while True:
        msg = c.poll(0.1)
        if msg is None:
            continue
        elif not msg.error():
            print('Received message: {0}'.format(msg.value()))
        elif msg.error().code() == KafkaError._PARTITION_EOF:
            print('End of partition reached {0}/{1}'
                  .format(msg.topic(), msg.partition()))
        else:
            print('Error occured: {0}'.format(msg.error().str()))
        time.sleep(2)

except KeyboardInterrupt:
    pass

finally:
    c.close()