# python2
from confluent_kafka import Consumer, KafkaError
import sys
import time
import os 
# import infer_distributed as inferdis
import numpy as np
import json
import zlib
import argparse
import yaml

class DelligenceConsumer(object) :
  def __init__(self, name) :
    self.name = name
    self.__parse_args()
    self.__load_config()
    self.__build_settings()
    self.consumer = Consumer(self.settings)

  def __parse_args(self):
    parser = argparse.ArgumentParser(description='End-to-end inference')
    parser.add_argument(
            '--cfginfer',
            dest='cfginfer',
            help='cfginfer model file (/path/to/config_infer.yaml)',
            default='config_infer.yaml',
            type=str
    )
    self.args, _ = parser.parse_known_args('--cgfinfer') 

  def __check_config(self, keys) :
    for key in keys :
      ks = key.split(".")
      v = self.cfginfer
      for k in ks :
        if v.get(k) is None :
          logging.error('%s is not found in config file : %s' % (k, self.args.cfginfer))
          sys.exit()
        v = v[k]

  def __load_config(self) :
    cf = open('config_infer.yaml', 'r')
    self.cfginfer = yaml.load(cf)

  def __build_settings(self) :
    settings = {
      'bootstrap.servers': self.cfginfer['KAFKA']['BOOTSTRAP']['SERVER'],
      'group.id': self.name,
      'client.id': self.name+"-"+str(os.getpid()),
      'enable.auto.commit': True,
      'session.timeout.ms': 6000,
      'default.topic.config': {'auto.offset.reset': 'smallest'}
    }
    self.settings = settings


class ConsumerITMS(DelligenceConsumer) :
  def __init__(self) :
    super(ConsumerITMS, self).__init__('ITMS')
    keys = [
      'DELLIGENCE.ITMS.INPUT' 
    ]
    self._DelligenceConsumer__check_config(keys)
    # self.inferdis = inferdis.InferCounting()
    
  def listen(self, idx) :
    self.consumer.subscribe(['ITMS-%d' % idx])
    try:
      while True:
        msg = self.consumer.poll(0.1)
        if msg is None:
          continue
        elif not msg.error():
          val = zlib.decompress(msg.value())
          jsonval = json.loads(val, encoding='latin-1')
          im = np.loads(jsonval['image'].encode('latin-1'))
          print('Processing : {0}'.format(im))
          # self.infer(im)

        elif msg.error().code() == KafkaError._PARTITION_EOF:
          print('End of partition reached {0}/{1}'
                .format(msg.topic(), msg.partition()))
        else:
          print('Error occured: {0}'.format(msg.error().str()))

    except KeyboardInterrupt:
      pass

    finally:
      self.consumer.close()

class ConsumerLPNR(DelligenceConsumer) :
  def __init__(self) :
    super(ConsumerLPNR, self).__init__('LPNR')
    keys = [
      'DELLIGENCE.LPNR.INPUT' 
    ]
    self._DelligenceConsumer__check_config(keys)
    # self.inferdis = inferdis.InferPlate()

  def listen(self) :
    self.consumer.subscribe(['LPNR'])
    try:
      while True:
        msg = self.consumer.poll(0.1)
        if msg is None:
          continue
        elif not msg.error():
          val = zlib.decompress(msg.value())
          jsonval = json.loads(val, encoding='latin-1')
          im = np.loads(jsonval['image'].encode('latin-1'))
          print('Processing : {0}'.format(im))
          # self.infer(im)

        elif msg.error().code() == KafkaError._PARTITION_EOF:
          print('End of partition reached {0}/{1}'
                .format(msg.topic(), msg.partition()))
        else:
          print('Error occured: {0}'.format(msg.error().str()))

    except KeyboardInterrupt:
      pass

    finally:
      self.consumer.close()
