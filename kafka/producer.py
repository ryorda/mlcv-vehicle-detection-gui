# python2 

from confluent_kafka import Producer
import json
import time
import sys
import yaml
import logging
import functools
from multiprocessing import Pool
import argparse
import cv2  # NOQA (Must import before importing caffe2 due to bug in cv2)
import numpy as np 
import zlib

# OpenCL may be enabled by default in OpenCV3; disable it because it's not
# thread safe and causes unwanted GPU memory allocations.
cv2.ocl.setUseOpenCL(False)


class DelligenceProducer(object):
	def __init__(self, name) :
		self.name = name
		self.__parse_args()
		self.__load_config()
		self.producer = Producer({
			'bootstrap.servers': self.cfginfer['KAFKA']['BOOTSTRAP']['SERVER']
			})

	def __parse_args(self):
		parser = argparse.ArgumentParser(description='End-to-end inference')
		parser.add_argument(
						'--cfginfer',
						dest='cfginfer',
						help='cfginfer model file (/path/to/config_infer.yaml)',
						default='config_infer.yaml',
						type=str
		)
		parser.add_argument(
            'input_folder', help='video or folder of videos', default='demo'
    )
		self.args = parser.parse_args() 

	def __load_config(self) :
		cf = open(self.args.cfginfer, 'r')
		self.cfginfer = yaml.load(cf)

	def __check_config(self, keys) :
		for key in keys :
			ks = key.split(".")
			v = self.cfginfer
			for k in ks :
				if v.get(k) is None :
					logging.error('%s is not found in config file : %s' % (k, self.args.cfginfer))
					sys.exit()
				v = v[k]

	def multiargs_wrapper(self, func, args) :
		func(*args)

	def capture_video(self, index, callback):
		video_name = self.cfginfer['DELLIGENCE'][self.name]['INPUT'][index]
		cap = cv2.VideoCapture(video_name)

		while(cap.isOpened()):
			ret, frame = cap.read()
			callback(frame)
			if (not ret) :
				break;

	def get_input_cams(self) :
		if self.cfginfer['DELLIGENCE'][self.name]['INPUT'] :
			return self.cfginfer['DELLIGENCE'][self.name]['INPUT'].keys()
		logging.warning('no input cams for %s' % self.name)
		return []

	def __acked(self, err, msg):
		if err is not None:
				logging.error("Failed to deliver message: {0}: {1}"
							.format(msg.value(), err.str()))
		else:
				logging.info("Message produced: {0}".format(msg.value()))

	def send(self, name, topic, image) :
		
		resized_image = image.copy() 
		if image.shape[0] > 800 or image.shape[1] > 800 :
			resized_image = cv2.resize(resized_image, (800, 600)) 

		data = {
			'image' : np.array(resized_image).dumps(),
			'timestamp' : time.time(),
			'name' : name,
			'topic' : topic
		}
		message = json.dumps(data, encoding='latin-1')
		compressed_message = zlib.compress(message)
		print(sys.getsizeof(compressed_message))
		self.producer.produce(topic, compressed_message, callback=self.__acked)

class ProducerITMS(DelligenceProducer) :
	def __init__(self) :
		super(ProducerITMS, self).__init__('ITMS')
		keys = [
			'DELLIGENCE.ITMS',
			'DELLIGENCE.ITMS.INPUT'	
		]
		self._DelligenceProducer__check_config(keys)


	def producing(self) :
		while True :
			cams = self.get_input_cams()
			# pool = Pool(len(cams))
			print('getting images')
			print(cams)

			args = []
			for idx in cams :
				bound_send = functools.partial(self.send, 'ITMS', 'ITMS-%d' % idx)
				self.capture_video(idx, bound_send)
			# 	args.append((idx, bound_send))


			# pool.map(functools.partial(self.multiargs_wrapper, self.capture_video), args)


class ProducerLPNR(DelligenceProducer) :
	def __init__(self) :
		super(ProducerLPNR, self).__init__('LPNR')
		keys = [
			'DELLIGENCE.LPNR',
			'DELLIGENCE.LPNR.INPUT'	
		]
		self._DelligenceProducer__check_config(keys)

	def producing(self) :
		while True :
			cams = self.get_input_cams()
			# pool = Pool(len(cams))
			print('getting images')
			print(cams)

			args = []
			for idx in cams :
				bound_send = functools.partial(self.send, 'LPNR', 'LPNR')
				self.capture_video(idx, bound_send)
			# 	args.append((idx, bound_send))


			# pool.map(functools.partial(self.multiargs_wrapper, self.capture_video), args)
