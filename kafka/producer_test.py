# python2 
from confluent_kafka import Producer
import sys

def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: {0}: {1}"
              .format(msg.value(), err.str()))
    else:
        print("Message produced: {0}".format(msg.value()))

p = Producer({'bootstrap.servers': 'localhost:9092'})

while True : 
	p.produce(sys.argv[1], raw_input('put your message :'), callback=acked)