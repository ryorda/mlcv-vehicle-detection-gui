var rethinkdb = require('rethinkdb');

var host = 'http://localhost';
var port = 28015;
var dbname = process.env.REACT_APP_NAME;

module.exports.listen = function(tablename, next){
	rethinkdb.connect( {host: host, port: port}, function(err, conn) {
    if (err) return next(err);
    rethinkdb.db(dbname).table(tablename).changes().run(conn, function(err, cursor){
    	if (err) return next(err);
    	cursor.toArray(function(err, result){
    		if (err) return next(err);
    		return next(null, result);
    	})
    })
	})
};

module.exports.get = function(tablename, next){
	console.log('get')
	rethinkdb.connect( {host: host, port: port}, function(err, conn) {
    console.log('connect')
    if (err) return next(err);
    rethinkdb.db(dbname).table(tablename).run(conn, function(err, cursor){
    	console.log('run')
    	if (err) return next(err);
    	cursor.toArray(function(err, result){
    		console.log(result)
    		if (err) return next(err);
    		return next(null, result);
    	})
    })
	})
};

module.exports.insert = function(tablename, data, next){
  rethinkdb.connect( {host: host, port: port}, function(err, conn) {
    if (err) return next(err);
		rethinkdb.db(dbname).table(tablename).insert(data).run(conn, function(err, result) {
    	if (err) return next(err);
	    console.info(JSON.stringify(result, null, 2));
	    return next(null);
		})
	})
}

module.exports.createTable = function(tablename, next){
  rethinkdb.connect( {host: host, port: port}, function(err, conn) {
    if (err) return next(err);
		rethinkdb.db(dbname).tableCreate(tablename).run(conn, function(err, result) {
    	if (err) return next(err);
	    console.info(JSON.stringify(result, null, 2));
	    return next(null);
		})
	})
}

module.exports.createDB = function(dbname, next){
  rethinkdb.connect( {host: host, port: port}, function(err, conn) {
    if (err) return next(err);
		rethinkdb.dbCreate(dbname).run(conn, function(err, result) {
    	if (err) return next(err);
	    console.info(JSON.stringify(result, null, 2));
	    return next(null);
		})
	})
}