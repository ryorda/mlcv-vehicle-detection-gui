var host = 'http://localhost';
var port = 12312;
var socketio = require('socket.io-client')(host + ":" + port);

module.exports.listen = function(state, next){
    socketio.on(state, function(err, props){
        next(err, props);
    })
};

module.exports.emit = function(state, value){
    socketio.emit(state, value);
}