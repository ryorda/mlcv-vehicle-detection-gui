require('asset/css/index.css');
require('semantic-ui-css/semantic.min.css');

var React = require('react');
var ReactDOM = require('react-dom');
var Router = require("react-router-dom").BrowserRouter;
var App = require('src/routes');
var registerServiceWorker = require('./registerServiceWorker').default;

ReactDOM.render(
	<Router>
    <App />
  </Router>
  , document.getElementById('root'));
registerServiceWorker();
