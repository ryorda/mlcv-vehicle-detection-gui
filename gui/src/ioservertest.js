const io = require('socket.io')();
var fs = require('fs');

io.on('connection', (client) => {
  client.on('count_info', (value) => {
  	console.log('listen count_info : ' + value);
  });
  var file = fs.readFileSync('asset/img/logo.png')

  setInterval(function(){
    var data = [
    {
      lane : 1,
      car : Math.floor(Math.random() * Math.floor(100)),
      bus : Math.floor(Math.random() * Math.floor(100)),
      truck : Math.floor(Math.random() * Math.floor(100))
    },
    {
      lane : 2,
      car : Math.floor(Math.random() * Math.floor(100)),
      bus : Math.floor(Math.random() * Math.floor(100)),
      truck : Math.floor(Math.random() * Math.floor(100))
    },
    {
      lane : 3,
      car : Math.floor(Math.random() * Math.floor(100)),
      bus : Math.floor(Math.random() * Math.floor(100)),
      truck : Math.floor(Math.random() * Math.floor(100))
    },
    {
      lane : 4,
      car : Math.floor(Math.random() * Math.floor(100)),
      bus : Math.floor(Math.random() * Math.floor(100)),
      truck : Math.floor(Math.random() * Math.floor(100))
    },
    {
      lane : 5,
      car : Math.floor(Math.random() * Math.floor(100)),
      bus : Math.floor(Math.random() * Math.floor(100)),
      truck : Math.floor(Math.random() * Math.floor(100))
    }
    ];
    console.log(data);
    client.emit('count_info', data)
    client.emit('count_image', file.toString('hex'))
  }, 1000);

  var mark = true;

  setInterval(function(){
  	var data = [
  	{
      plate : 1,
      candidate : [
        {
          plate : 'B12345XX',
          confidence : Math.random()
        },
        {
          plate : 'B12345YY',
          confidence : Math.random()
        },
      ]
    },
    {
      plate : 2,
      candidate : [
        {
          plate : 'B12345ZZ',
          confidence : Math.random()
        }
      ]
    },
    {
      plate : 3,
      candidate : []
    }
  	];
  	console.log(data);
    var hexstring = file.toString('hex')
    if (mark) hexstring = '';
    mark = !mark;
  	client.emit('plate_info', data)
  	client.emit('plate_image', hexstring)
  }, 200);
});

const port = 12312;
io.listen(port);
console.log('listening on port ', port);