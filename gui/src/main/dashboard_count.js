var createReactClass = require("create-react-class");
var React = require("react");
var ReactDOM = require("react-dom")

// custom component

// semantic component
var Sidebar = require('semantic-ui-react').Sidebar;
var Menu = require('semantic-ui-react').Menu;
var Image = require('semantic-ui-react').Image;
var Icon = require('semantic-ui-react').Icon;

// others 
var socketioclient = require('src/util/socketioclient');
var logo = require('asset/img/logo.png');

var Component = createReactClass({
	getInitialState : function(){
		return {
			sidebar : (<div></div>),
			counting : [], // [{ lane : "...", car : "...", truck : "...", bus : "..."}],
			contentImage : ''
		}
	},
	componentDidMount : function(){
		socketioclient.listen('count_info', function(data){
			this.setState({ counting : data});
		}.bind(this));
		
		socketioclient.listen('count_image', function(data){
			this.setState({ contentImage : data});
		}.bind(this));

		// it will trigger webserver.py to send counting and contentImage
		setInterval(socketioclient.emit.bind(null, 'count', true), 1000);
	},
	renderMenuItems : function(){
		var counting = this.state.counting;
		var items = [];
		for (var i in counting){
			items.push(
					<Menu.Item>
						<Menu.Header> {'Lane ' + counting[i].lane} </Menu.Header>
						<Menu.Menu>
							<Menu.Item name={'lane_' + counting[i].lane + ":car"}>
								<div>
									<Icon name='car'/>
									<span> {counting[i].car ? counting[i].car : 0} </span>
								</div>
							</Menu.Item>
							<Menu.Item name={'lane_' + counting[i].lane + ":bus"}>
								<div>
									<Icon name='bus'/>
									<span> {counting[i].bus ? counting[i].bus : 0} </span>
								</div>
							</Menu.Item>
							<Menu.Item name={'lane_' + counting[i].lane + ":truck"}>
								<div>
									<Icon name='truck'/>
									<span> {counting[i].truck ? counting[i].truck : 0} </span>
								</div>
							</Menu.Item>
						</Menu.Menu>
					</Menu.Item>
				);
		}
		return items;
	},
	handleSidebarRef : function(ref){
		this.setState({ sidebar : ReactDOM.findDOMNode(ref) });
	},
	getBufferImage : function(){
		if (this.state.contentImage === '')
			return '';
		var img = Buffer.from(this.state.contentImage, 'hex').toString('base64');
		var style = {
			height : (this.state.sidebar.clientHeight) + 'px',
			width : '100%',
			margin : '0px',
			padding : '0px',
		}
		return (
			<Image src={"data:image/png;base64," + img} style={style}/>
			);
	},
	render : function(){
		return (
			<div>
				<Sidebar as={Menu} visible vertical style={{ height: '100%', position: 'absolute', margin : '0px' }} ref={this.handleSidebarRef}>
					<Menu.Item name='logo'>
             <Image src={logo} size='tiny' centered/>
             <h4 align="center"> Telkom - BPJT </h4>
          </Menu.Item>
          {this.renderMenuItems()}
				</Sidebar>
				<div style={{ marginLeft : (this.state.sidebar.clientWidth) + 'px'}}>
						{this.getBufferImage()}
				</div>
			</div>	
			);
	}
});

module.exports = Component;