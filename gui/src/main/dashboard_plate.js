var createReactClass = require("create-react-class");
var React = require("react");
var ReactDOM = require("react-dom")

// custom component

// semantic component
var Sidebar = require('semantic-ui-react').Sidebar;
var Menu = require('semantic-ui-react').Menu;
var Image = require('semantic-ui-react').Image;
var Icon = require('semantic-ui-react').Icon;

// others 
var socketioclient = require('src/util/socketioclient');
var logo = require('asset/img/logo.png');

var Component = createReactClass({
	getInitialState : function(){
		return {
			sidebar : (<div></div>),
			info : [], // [{ lane : "...", car : "...", truck : "...", bus : "..."}],
			contentImage : ''
		}
	},
	componentDidMount : function(){
		socketioclient.listen('plate_info', function(data){
			this.setState({ info : data});
		}.bind(this));
		
		socketioclient.listen('plate_image', function(data){
			this.setState({ contentImage : data});
		}.bind(this));

		// it will trigger webserver.py to send info and contentImage
		setInterval(socketioclient.emit.bind(null, 'plate', true), 1000);
	},
	renderMenuItems : function(){
		var info = this.state.info;
		if (!info || info.length === 0) 
			return ' - ';

		var items = [];
		for (var i in info){
			var candidates = []
			var cands = info[i].candidate
			for (var cand in cands){
				var rank = Number(cand) + 1
				candidates = candidates.concat([
					<Menu.Item key={'plate_' + info[i].plate + 'rank_' + rank + ":plate"} name={'plate_' + info[i].plate + 'rank_' + rank + ":plate"}>
						<div>
							<span> {rank + '. '} </span>
							<span> {'plate : ' + cands[cand].plate} </span>
						</div>
					</Menu.Item>,
					<Menu.Item key={'plate_' + info[i].plate + 'rank_' + rank + ":confidence"} name={'plate_' + info[i].plate + 'rank_' + rank + ":confidence"}>
						<div>
							<Icon name='bus' style={{ visibility : 'hidden'}}/>
							<span> {'confidence : ' + cands[cand].confidence.toFixed(5)} </span>
						</div>
					</Menu.Item>
					])
			}

			if (!candidates || candidates.length === 0)
				candidates = (
					<Menu.Item name={'plate_' + info[i].plate + 'rank_' + rank + ":plate"}>
						<div>
							<span> - </span>
						</div>
					</Menu.Item>
					)

			items.push(
					<Menu.Item key={'Plat #' + info[i].plate}>
						<Menu.Header> {'Plat #' + info[i].plate} </Menu.Header>
						<Menu.Menu>
							{candidates}
						</Menu.Menu>
					</Menu.Item>
				);
		}
		return items;
	},
	handleSidebarRef : function(ref){
		this.setState({ sidebar : ReactDOM.findDOMNode(ref) });
	},
	getBufferImage : function(){
		if (this.state.contentImage === '')
			return '';
		var img = Buffer.from(this.state.contentImage, 'hex').toString('base64');
		var style = {
			height : (this.state.sidebar.clientHeight) + 'px',
			width : '100%',
			margin : '0px',
			padding : '0px',
		}
		return (
			<Image src={"data:image/png;base64," + img} style={style}/>
			);
	},
	render : function(){
		return (
			<div>
				<Sidebar as={Menu} visible vertical style={{ height: '100%', position: 'absolute', margin : '0px' }} ref={this.handleSidebarRef}>
					<Menu.Item name='logo'>
             <Image src={logo} size='tiny' centered/>
             <h4 align="center"> Telkom - BPJT </h4>
          </Menu.Item>
          <Menu.Item>
          	{this.renderMenuItems()}
          </Menu.Item>
				</Sidebar>
				<div style={{ marginLeft : (this.state.sidebar.clientWidth) + 'px'}}>
						{this.getBufferImage()}
				</div>
			</div>	
			);
	}
});

module.exports = Component;