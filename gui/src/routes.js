var createReactClass = require("create-react-class");
var React = require('react');
var RouterDOM = require('react-router-dom');

// BPJT components
var PageDashboardCount = require('src/main/dashboard_count');
var PageDashboardPlate = require('src/main/dashboard_plate');

var router = createReactClass({

  render : function(){
  	return (
		    <RouterDOM.Switch>
		      <RouterDOM.Route exact path="/" component={PageDashboardCount}/>
		      <RouterDOM.Route exact path="/count" component={PageDashboardCount}/>
		      <RouterDOM.Route exact path="/plate" component={PageDashboardPlate}/>
		    </RouterDOM.Switch>
		  );
  }
});

module.exports = router;