from multiprocessing import Pool
from functools import partial
import pathos.pools as pp

class Test(object):
	def __init__(self) :
		self.attr = 'attr'

	def test(self, a, b, c) :
		print(a, "-", b, "-", c)

	def wrapper(self, func, args) :
		func(*args)

t = Test()
# pool = Pool(2)
pool = pp.ProcessPool(2)
pool.map(partial(t.wrapper, t.test), [(1, 2, 3), (3, 4, 5), (4, 5, 6)])
# pool.apply_async(partial(t.wrapper, t.test), (1, 2, 3))
# pool.apply_async(partial(t.wrapper, t.test), (3, 4, 5))
# pool.apply_async(partial(t.wrapper, t.test), (4, 5, 6


class Test2(Test) :
	def __init__(self) :
		print(self.attr)