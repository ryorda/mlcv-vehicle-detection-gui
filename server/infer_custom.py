#!/usr/bin/env python2

# Copyright (c) 2017-present, Facebook, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################

"""Perform inference on a single image or all images with a certain extension
(e.g., .jpg) in a folder.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from collections import defaultdict
import argparse
import cv2  # NOQA (Must import before importing caffe2 due to bug in cv2)
import glob
import logging
import os
import sys
import time

from caffe2.python import workspace

from core.config import assert_and_infer_cfg
from core.config import cfg
from core.config import merge_cfg_from_file
from utils.timer import Timer
import core.test_engine as infer_engine
import datasets.dummy_datasets as dummy_datasets
import utils.c2 as c2_utils
import utils.logging
import utils.vis as vis_utils
data_counting = []
data_png = []

c2_utils.import_detectron_ops()
# OpenCL may be enabled by default in OpenCV3; disable it because it's not
# thread safe and causes unwanted GPU memory allocations.
cv2.ocl.setUseOpenCL(False)
logger = logging.getLogger(__name__)

def parse_args():
        parser = argparse.ArgumentParser(description='End-to-end inference')
        parser.add_argument(
                '--cfg',
                dest='cfg',
                help='cfg model file (/path/to/model_config.yaml)',
                default='configs/12_2017_baselines/config.yaml',
                type=str
        )
        parser.add_argument(
                '--wts',
                dest='weights',
                help='weights model file (/path/to/model_weights.pkl)',
                default='https://s3-us-west-2.amazonaws.com/detectron/35861858/12_2017_baselines/e2e_mask_rcnn_R-101-FPN_2x.yaml.02_32_51.SgT4y1cO/output/train/coco_2014_train:coco_2014_valminusminival/generalized_rcnn/model_final.pkl',
                type=str
        )
        parser.add_argument(
                '--output-dir',
                dest='output_dir',
                help='directory for visualization pdfs (default: /tmp/infer_custom)',
                default='/tmp/infer_custom',
                type=str
        )
        parser.add_argument(
                '--video-ext',
                dest='video_ext',
                help='video file name extension (default: mp4)',
                default='mp4',
                type=str
        )
        parser.add_argument(
                'video_or_folder', help='video or folder of videos', default='demo'
        )
        # if len(sys.argv) == 1:
        #        parser.print_help()
        #        sys.exit(1)
        return parser.parse_args()

def infer(im, model, dataset, video_name, output_dir, last_count, memory, frame_num=1) :
        timers = defaultdict(Timer)
        t = time.time()
        with c2_utils.NamedCudaScope(0):
                cls_boxes, cls_segms, cls_keyps = infer_engine.im_detect_all(
                        model, im, None, timers=timers
                )
        logger.info('Inference time: {:.3f}s'.format(time.time() - t))
        for k, v in timers.items():
                logger.info(' | {}: {:.3f}s'.format(k, v.average_time))

        img, count, memory = vis_utils.vis_one_image(
                        im[:, :, ::-1], # BGR -> RGB for visualization
                        video_name + ' ' + str(frame_num),
                        output_dir,
                        cls_boxes,
                        cls_segms,
                        cls_keyps,
                        last_count=last_count,
                        memory=memory,
                        dataset=dataset,
                        box_alpha=0.3,
                        show_class=True,
                        thresh=0.7,
                        kp_thresh=2,
                        ext='png'
                )
        return img, count, memory

def detectron(args):
        global data_counting
        global data_png

        merge_cfg_from_file(args.cfg)
        cfg.TEST.WEIGHTS = args.weights
        cfg.NUM_GPUS = 1
        assert_and_infer_cfg()
        model = infer_engine.initialize_model_from_cfg()
        dummy_coco_dataset = dummy_datasets.get_coco_dataset()

        if os.path.isdir(args.video_or_folder):
                video_list = glob.iglob(args.video_or_folder + '/*.' + args.video_ext)
        else:
                video_list = [args.video_or_folder]

        for i, video_name in enumerate(video_list):
                out_name = os.path.join(
                        args.output_dir, '{}'.format(os.path.basename(video_name) + '.png')
                )
                logger.info('Processing {} -> {}'.format(video_name, out_name))

                cap = cv2.VideoCapture(video_name)

                count_frame = 1
                counting = None
                memory = []
                while(cap.isOpened()):
                        ret, frame = cap.read()

                        if (not ret) :
                                break;

                        print('count_frame ' + str(count_frame))
                        count_frame += 1
                        im, counting, memory = infer(frame, model, dummy_coco_dataset, video_name, args.output_dir, counting, memory, count_frame)
                        png = cv2.imencode('.png', im)[1]

                        clean_count = []
                        for k, v in counting.iteritems() :
                                clean_count.append(v)
                        
                        data_counting = clean_count
                        print(data_counting)
                        data_png = png
                        


def main() :
        print('main')
        workspace.GlobalInit(['caffe2', '--caffe2_log_level=0'])
        utils.logging.setup_logging(__name__)
        args = parse_args()
        detectron(args)

if __name__ == '__main__':
        main()