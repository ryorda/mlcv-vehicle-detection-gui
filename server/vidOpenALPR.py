import numpy as np
import cv2
import csv
from openalpr import Alpr

candidate = []
alpr = Alpr("eu", "openalpr.conf", "runtime_data")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)

alpr.set_top_n(1)
#alpr.set_default_region("")

#cap = cv2.VideoCapture("http://91.190.227.198/mjpg/video.mjpg")
cap = cv2.VideoCapture("vid1.mp4")

csv = open("result_10skip_id.csv", "w")
columnTitleRow = " ,plateno, confidence%\n"
csv.write(columnTitleRow) 

#################### Setting up the file ################
success,image = cap.read()

#################### Setting up parameters ################

#OpenCV is notorious for not being able to good to 
# predict how many frames are in a video. The point here is just to 
# populate the "desired_frames" list for all the individual frames
# you'd like to capture. 

fps = cap.get(cv2.CAP_PROP_FPS)
est_video_length_minutes = 40         # Round up if not sure.
est_tot_frames = est_video_length_minutes * 20 * fps  # Sets an upper bound # of frames in video clip

n = 10                             # Desired interval of frames to include
desired_frames = n * np.arange(est_tot_frames) 


#################### Initiate Process ################

for i in desired_frames:
    cap.set(1,i-1)                      
    #success,image = cap.read(1)         # image is an array of array of [R,G,B] values
    #frameId = cap.get(1)                # The 0th frame is often a throw-away
    #cv2.imwrite("FolderFrames/frame%d.jpg" % frameId, image)
#while(True):    
    ret, frame = cap.read(1) 

    if ret:        
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
	imCrop = frame[200:1090, 0:1920]
        cv2.imwrite("img.jpg", imCrop)

        results = alpr.recognize_file("img.jpg")

        i = 0
        
        for plate in results['results']:
            i += 1
            print("Plate #%d" % i)
            print("   %12s %12s" % ("Plate", "Confidence"))
            for candidate in plate['candidates']:
                prefix = "-"
                if candidate['matches_template']:
                    prefix = "*"

                print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
		row = prefix + "," + candidate['plate']+ "," + str(candidate['confidence']) + "\n"
		csv.write(row)
		#a = np.asarray([prefix, candidate['plate'], candidate['confidence']])
		#np.savetxt('testid_skip10.csv', a, delimiter=",", fmt="%s")
		      
    else:
        break;

    

# When everything done, release the capture
cap.release()
alpr.unload()
cv2.destroyAllWindows()
