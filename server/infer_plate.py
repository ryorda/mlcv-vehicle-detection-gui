#!/usr/bin/env python2

# Copyright (c) 2017-present, Facebook, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################

"""Perform inference on a single image or all images with a certain extension
(e.g., .jpg) in a folder.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import cv2  # NOQA (Must import before importing caffe2 due to bug in cv2)
import glob
import logging
import os
import sys
import time
from openalpr import Alpr


alpr = Alpr("eu", "openalpr.conf", "runtime_data")

data_info = []
data_png = []

logger = logging.getLogger(__name__)

def parse_args():
	parser = argparse.ArgumentParser(description='End-to-end inference')
	
	parser.add_argument(
		'video_or_folder', help='video or folder of videos', default='demo'
	)
	# if len(sys.argv) == 1:
	#	parser.print_help()
	#	sys.exit(1)
	return parser.parse_args()

def infer(im) :
	imCrop = im[200:1090, 0:1920]
	cv2.imwrite("infer_img.jpg", imCrop)

	results = alpr.recognize_file("infer_img.jpg")

	info = []
	i = 0
	for plate in results['results']:
		i += 1
		temp = { 
			'plate' : i,	
			'candidate' : []
		}
		for candidate in plate['candidates']:
			temp['candidate'].append({
					'plate' : candidate['plate'],
					'confidence' : candidate['confidence']
				});
		info.append(temp)

	return im, info


def detect(args):
	global data_info
	global data_png

	if os.path.isdir(args.video_or_folder):
		video_list = glob.iglob(args.video_or_folder + '/*.' + args.video_ext)
	else:
		video_list = [args.video_or_folder]

	for i, video_name in enumerate(video_list):
		logger.info('Processing Plate {}'.format(video_name))

		cap = cv2.VideoCapture(video_name)

		count_frame = 1
		info = []
		while(cap.isOpened()):
			ret, frame = cap.read()

			if (not ret) :
				break

			print('count_frame ' + str(count_frame))
			count_frame += 1

			t = time.time()
			im, info = infer(frame)
			im = cv2.resize(im, (640, 480), interpolation = cv2.INTER_LINEAR)
			time.sleep(1)
			print( 'elapsed : ' + str(time.time() - t) + ' s')
			png = cv2.imencode('.png', im)[1]
			
			if (len(info) > 0) :
					data_info = info
			data_png = png
			
			print(data_info)


def main() :
	args = parse_args()
	if not alpr.is_loaded():
		print("Error loading OpenALPR")
		sys.exit(1)
	detect(args)

if __name__ == '__main__':
	main()