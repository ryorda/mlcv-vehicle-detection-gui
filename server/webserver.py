from flask import Flask, render_template
from flask_socketio import SocketIO
# import infer_custom as infer_count
import infer_plate as infer_plate
import binascii
import random
import thread

app = Flask(__name__)
socketio = SocketIO(app)

@socketio.on('connect')
def connect():
	print('connect')

# @socketio.on('count')
# def count(data):
# 	print('count')
# 	hexstring = binascii.hexlify(bytearray(infer.data_png))
# 	socketio.emit('count_image', hexstring)
# 	socketio.emit('count_info', infer_count.data_counting)

@socketio.on('plate')
def plate(data):
	print('plate')
	hexstring = binascii.hexlify(bytearray(infer_plate.data_png))
	socketio.emit('plate_image', hexstring)
	socketio.emit('plate_info', infer_plate.data_info)

if __name__ == '__main__':
	# thread.start_new_thread(infer_count.main, ())
	thread.start_new_thread(infer_plate.main, ())
	print('listen to 12312')
	socketio.run(app, host='127.0.0.1', port = 12312)
