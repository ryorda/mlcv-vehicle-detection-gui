#!/usr/bin/env python2

# Copyright (c) 2017-present, Facebook, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################

"""Perform inference on a single image or all images with a certain extension
(e.g., .jpg) in a folder.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from collections import defaultdict
import argparse
import cv2  # NOQA (Must import before importing caffe2 due to bug in cv2)
import glob
import logging
import os
import sys
import time

from caffe2.python import workspace

from core.config import assert_and_infer_cfg
from core.config import cfg
from core.config import merge_cfg_from_file

from utils.timer import Timer

import core.test_engine as infer_engine
import datasets.dummy_datasets as dummy_datasets
import utils.c2 as c2_utils
import utils.logging
import utils.vis as vis_utils

import pymongo
import binascii
import os
import yaml

# global variables
args = {}
logger = None
model_itms = None
dummy_coco_dataset_itms = None

class DelligenceInference(object) :
  def __init__(self) :
    self.__load_config
    keys = [
      'DATABASE.URI',
      'DATABASE.DBNAME',
    ]
    self.__check_config(keys)
    self.mongo_client = pymongo.MongoClient(self.cfginfer['DATABASE']['URI'])
    
  def __load_config(self) :
    cf = open(self.args.cfginfer, 'r')
    self.cfginfer = yaml.load(cf)

  def __check_config(self, keys) :
    for key in keys :
      ks = key.split(".")
      v = self.cfginfer
      for k in ks :
        if v.get(k) is None :
          logging.error('%s is not found in config file : %s' % (k, self.args.cfginfer))
          sys.exit()
        v = v[k]

  # upload result to database
  def __upload_result(collection, data):
    self.mongo_client[self.cfginfer['DATABASE']['DBNAME']][collection].insert_one(data)

class InferCounting(DelligenceInference) :
  def __init__(self) :
    self.content = []
    self.image = []
    self.last_count = None
    self.memory = []

    merge_cfg_from_file(args.cfg)
    cfg.TEST.WEIGHTS = args.weights
    cfg.NUM_GPUS = 1
    assert_and_infer_cfg()
    self.model = infer_engine.initialize_model_from_cfg()
    self.dataset = dummy_datasets.get_coco_dataset()

  
  def infer(self, im, get_result=False) :

    timers = defaultdict(Timer)
    t = time.time()
    with c2_utils.NamedCudaScope(0):
            cls_boxes, cls_segms, cls_keyps = infer_engine.im_detect_all(
                    self.model, im, None, timers=timers
            )
    logger.info('Inference time: {:.3f}s'.format(time.time() - t))
    for k, v in timers.items():
            logger.info(' | {}: {:.3f}s'.format(k, v.average_time))

    img, self.last_count, self.memory = vis_utils.vis_one_image(
        im[:, :, ::-1], # BGR -> RGB for visualization
        'video_name',
        'output_dir',
        cls_boxes,
        cls_segms,
        cls_keyps,
        last_count=self.last_count,
        memory=self.memory,
        dataset=self.dataset,
        box_alpha=0.3,
        show_class=True,
        thresh=0.7,
        kp_thresh=2,
        ext='png'
      )

    jpg = cv2.imencode('.jpg', img)[1]
    img_hex = binascii.hexlify(bytearray(jpg))

    clean_count = []
    for k, v in self.last_count.iteritems() :
      clean_count.append(v)

    data = {
      "image_hex" : img_hex,
      "result" : clean_count,
      "timestamp" : time.time()
    }
    self._DelligenceInference__upload_result(args.mongo_db, 'itms', data)

    self.image = img
    self.content = clean_count

    if get_result :
      return img, clean_count


class InferPlate(DelligenceInference) :

  def __init__(self) :
    self.content = []
    self.image = []

    self.model = infer_engine.initialize_model_from_cfg()
    self.dataset = dummy_datasets.get_coco_dataset()


  def infer(self, im, get_result=False) :
    imCrop = im[200:1090, 0:1920]
    cv2.imwrite("infer_img%s.jpg" % str(os.getppid()), imCrop)

    results = alpr.recognize_file("infer_img.jpg")

    info = []
    i = 0
    for plate in results['results']:
      i += 1
      temp = { 
        'plate' : i,  
        'candidate' : []
      }
      for candidate in plate['candidates']:
        temp['candidate'].append({
            'plate' : candidate['plate'],
            'confidence' : candidate['confidence']
          });
      info.append(temp)

    jpg = cv2.imencode('.jpg', im)[1]
    img_hex = binascii.hexlify(bytearray(jpg))

    data = {
      "image_hex" : img_hex,
      "result" : info,
      "timestamp" : time.time()
    }
    self._DelligenceInference__upload_result(args.mongo_db, 'lpnr', data)

    if get_result :
      return im, info

# main function

def parse_args():
  parser = argparse.ArgumentParser(description='End-to-end inference')
  parser.add_argument(
          '--cfg',
          dest='cfg',
          help='cfg model file (/path/to/model_config.yaml)',
          default='configs/12_2017_baselines/config.yaml',
          type=str
  )
  parser.add_argument(
          '--wts',
          dest='weights',
          help='weights model file (/path/to/model_weights.pkl)',
          default='https://s3-us-west-2.amazonaws.com/detectron/35861858/12_2017_baselines/e2e_mask_rcnn_R-101-FPN_2x.yaml.02_32_51.SgT4y1cO/output/train/coco_2014_train:coco_2014_valminusminival/generalized_rcnn/model_final.pkl',
          type=str
  )
  # if len(sys.argv) == 1:
  #        parser.print_help()
  #        sys.exit(1)
  return parser.parse_args()                        


def init_global() :
    global args
    workspace.GlobalInit(['caffe2', '--caffe2_log_level=0'])
    utils.logging.setup_logging(__name__)
    args = parse_args()
    c2_utils.import_detectron_ops()
    # OpenCL may be enabled by default in OpenCV3; disable it because it's not
    # thread safe and causes unwanted GPU memory allocations.
    cv2.ocl.setUseOpenCL(False)
    logger = logging.getLogger(__name__)

init_global()